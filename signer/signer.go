package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
)

func SingleHash(in, out chan interface{}) {
	var wg = sync.WaitGroup{}
	var mut = sync.Mutex{}
	for data := range in {
		wg.Add(1)
		go GetSingleHash(&wg, &mut, out, fmt.Sprintf("%v", data))
	}
	wg.Wait()
}

func GetSingleHash(wg *sync.WaitGroup, mut *sync.Mutex, out chan interface{}, data string) {
	defer wg.Done()
	fmt.Println(data + " SingleHash data " + data)
	mut.Lock()
	shMD5 := DataSignerMd5(data)
	mut.Unlock()
	fmt.Println(data + " SingleHash md5(data) " + shMD5)
	var wg1 sync.WaitGroup
	var shCrc32MD5 string
	var shCrc32 string
	for i := 0; i < 2; i++ {
		wg1.Add(1)
		if i == 0 {
			go func(wg *sync.WaitGroup) {
				defer wg.Done()
				result := DataSignerCrc32(shMD5)
				shCrc32MD5 = result
			}(&wg1)
		} else {
			go func(wg *sync.WaitGroup) {
				defer wg.Done()
				result := DataSignerCrc32(data)
				shCrc32 = result
			}(&wg1)
		}
	}
	wg1.Wait()
	result := shCrc32 + "~" + shCrc32MD5
	fmt.Println(data + " SingleHash crc32(data) " + shCrc32)
	fmt.Println(data + " SingleHash crc32(md5(data)) " + shCrc32MD5)
	fmt.Println(data + " SingleHash result " + result)
	out <- result
}

func MultiHash(in, out chan interface{}) {
	var wg sync.WaitGroup
	for data := range in {
		wg.Add(1)
		go GetMultiHash(&wg, out, fmt.Sprintf("%v", data))
	}
	wg.Wait()
}

func GetMultiHash(wg *sync.WaitGroup, out chan interface{}, data string) {
	defer wg.Done()
	var wg1 sync.WaitGroup
	var mas [6]string
	for th := 0; th < 6; th++ {
		wg1.Add(1)
		go func(wg *sync.WaitGroup, th int, data string) {
			defer wg.Done()
			result := DataSignerCrc32(strconv.Itoa(th) + data)
			mas[th] = result
			fmt.Println(data + " MultiHash: crc32(th+step1)) " + strconv.Itoa(th) + " " + result)
		}(&wg1, th, data)
	}
	wg1.Wait()
	result := strings.Join(mas[:], "")
	out <- result
}

func ExecutePipeline(hashSignJobs ...job) {
	var wg sync.WaitGroup
	in := make(chan interface{})
	for _, task := range hashSignJobs {
		wg.Add(1)
		out := make(chan interface{})
		go doWork(&wg, in, out, task)
		in = out
	}
	wg.Wait()
}

func doWork(wg *sync.WaitGroup, in, out chan interface{}, task job) {
	defer wg.Done()
	defer close(out)
	task(in, out)
}

func CombineResults(in, out chan interface{}) {
	var result []string

	for i := range in {
		result = append(result, i.(string))
	}

	sort.Strings(result)
	out <- strings.Join(result, "_")
}
